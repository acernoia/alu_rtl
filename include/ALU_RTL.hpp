#ifndef ALU_RTL_HPP
#define ALU_RTL_HPP

#include "comparatore.hpp"
#include "nand.hpp"
#include "sommatore.hpp"
#include "MUX_4x16.hpp"

using namespace std;

SC_MODULE(ALU_RTL)
{
   sc_in<sc_uint<16> > operando1;
   sc_in<sc_uint<16> > operando2;
   sc_in<sc_uint<2> > funzione;
   sc_out<sc_uint<16> > risultato;

   sc_signal<sc_uint<16> > op1,op2,res;
   sc_signal<sc_uint<2> > sel;
   sc_signal<sc_uint<16> > adder_out,comp_out,nand_out;


   comparatore comp;
   nand nand1;
   sommatore adder;
   MUX_4x16 mux;

   ALU_RTL(sc_module_name name);

};


#endif
