#ifndef NAND_HPP
#define NAND_HPP

SC_MODULE(nand)
{
   sc_in<sc_uint<16> >  src1;
   sc_in<sc_uint<16> >  src2;
   sc_out<sc_uint<16> > out;

   SC_CTOR(nand)
   {
     SC_THREAD(calcola);
     sensitive << src1 << src2;
   }
   private:
   void calcola ();
};


#endif
