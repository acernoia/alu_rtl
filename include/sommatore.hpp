#ifndef SOMMATORE_HPP
#define SOMMATORE_HPP

SC_MODULE(sommatore)
{
   sc_in<sc_uint<16> >  src1;
   sc_in<sc_uint<16> >  src2;
   sc_out<sc_uint<16> > out;

   SC_CTOR(sommatore)
   {
     SC_THREAD(somma);
     sensitive << src1 << src2;
   }
   private:
   void somma ();
};


#endif
