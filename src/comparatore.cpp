#include <systemc.h>
#include "comparatore.hpp"

using namespace std;

void comparatore::compara()
{

   sc_bv<16> int1;
   sc_bv<16> int2;
   sc_bv<16> res;

   while(true)
   {
     wait();
     int1=src1->read();
     int2=src2->read();
     res[0]=((~int1[0])&(~int2[0]))|(int1[0]&int2[0]);
     res[1]=((~int1[1])&(~int2[1]))|(int1[1]&int2[1]);
     res[2]=((~int1[2])&(~int2[2]))|(int1[2]&int2[2]);
     res[3]=((~int1[3])&(~int2[3]))|(int1[3]&int2[3]);
     res[4]=((~int1[4])&(~int2[4]))|(int1[4]&int2[4]);
     res[5]=((~int1[5])&(~int2[5]))|(int1[5]&int2[5]);
     res[6]=((~int1[6])&(~int2[6]))|(int1[6]&int2[6]);
     res[7]=((~int1[7])&(~int2[7]))|(int1[7]&int2[7]);
     res[8]=((~int1[8])&(~int2[8]))|(int1[8]&int2[8]);
     res[9]=((~int1[9])&(~int2[9]))|(int1[9]&int2[9]);
     res[10]=((~int1[10])&(~int2[10]))|(int1[10]&int2[10]);
     res[11]=((~int1[11])&(~int2[11]))|(int1[11]&int2[11]);
     res[12]=((~int1[12])&(~int2[12]))|(int1[12]&int2[12]);
     res[13]=((~int1[13])&(~int2[13]))|(int1[13]&int2[13]);
     res[14]=((~int1[14])&(~int2[14]))|(int1[14]&int2[14]);
     res[15]=((~int1[15])&(~int2[15]))|(int1[15]&int2[15]);
     out->write(res.and_reduce());
   }
}
