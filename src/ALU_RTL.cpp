#include <systemc.h>
#include "ALU_RTL.hpp"

using namespace std;


SC_HAS_PROCESS(ALU_RTL);

ALU_RTL::ALU_RTL(sc_module_name name):

  sc_module(name),
  comp("comp"), mux("mux"), adder("adder"), nand1("nand1") 


   {
      //Collegamenti del comparatore
      comp.src1(this->operando1);
      comp.src2(this->operando2);
      comp.out(this->comp_out);
      //Collegmaneti della nand
      nand1.src1(this->operando1);
      nand1.src2(this->operando2);
      nand1.out(this->nand_out);
      //Collegamenti del sommatore
      adder.src1(this->operando1);
      adder.src2(this->operando2);
      adder.out(this->adder_out);
      //Collegamenti del mux
      mux.X0(this->adder_out);
      mux.X1(this->nand_out);
      mux.X2(this->operando1);
      mux.X3(this->comp_out);
      mux.sel(this->funzione);
      mux.Y(this->risultato);
   }
