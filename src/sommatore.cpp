#include <systemc.h>
#include "sommatore.hpp"

using namespace std;

void sommatore::somma()
{
   sc_uint<16> res;
   while(true)
   {
     wait();
     res=src1->read()+src2->read(); 
     out->write(res);
   }
}
