#include <systemc.h>
#include "MUX_4x16.hpp"

using namespace std;

void MUX_4x16::select()
{

   while(true)
   {
     wait();
     switch (sel->read())
     {
       case 0 : Y->write(X0); break;
       case 1 : Y->write(X1); break;
       case 2 : Y->write(X2); break;
       case 3 : Y->write(X3); break;
      }
   }

}
