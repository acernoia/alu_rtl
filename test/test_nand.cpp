#include <systemc.h>
#include <string>
#include "nand.hpp"

using namespace std;

SC_MODULE(TestBench)
{
    sc_signal<sc_uint<16> > operand1;
    sc_signal<sc_uint<16> > operand2;
    sc_signal<sc_uint<16> > result;
    nand nand1;
    
    SC_CTOR(TestBench) : nand1("nand1")
    {
        SC_THREAD(stimulus_thread);
        nand1.src1(this->operand1);
        nand1.src2(this->operand2);
        nand1.out(this->result);
        init_values();
    }

    int check() 
    {

        for (unsigned i=0;i<TEST_SIZE;i++) 
        {
            if (dato_letto[i] != risultato_test[i])
            {
                cout << "TEST FALLITO: " << i << endl;
                cout << "RISULTATO TEORICO: " << risultato_test[i] << endl;
                cout << "RISULTATO TEST :" << dato_letto[i] << endl;
                return 1;
             }
        }
        cout << "TEST OK" << endl;                 
        return 0;
    }


  private:

   void stimulus_thread() 
   {
        for (unsigned i=0;i<TEST_SIZE;i++) 
        {
            operand1.write(op1_test[i]);
            cout << "Operando 1 = " << op1_test[i] << endl;
            operand2.write(op2_test[i]);
            cout << "Operando 2 = " << op2_test[i] << endl;
            wait(1,SC_NS);
            dato_letto[i] = result.read(); 
            cout << "DATO LETTO : " << dato_letto[i] << endl << endl;
        }
    }

    static const unsigned TEST_SIZE = 8;
    // NOTA: short = 2 byte
    unsigned short op1_test[TEST_SIZE];
    unsigned short op2_test[TEST_SIZE];
    unsigned short risultato_test[TEST_SIZE];
    unsigned short dato_letto[TEST_SIZE];

    void init_values() 
    {
        op1_test[0] = 1;
        op1_test[1] = 5;
        op1_test[2] = 0;
        op1_test[3] = 4;
        op1_test[4] = 0;
        op1_test[5] = 239;
        op1_test[6] = 666;
        op1_test[7] = 65535;

        op2_test[0] = 0;
        op2_test[1] = 3;
        op2_test[2] = 0;
        op2_test[3] = 0;
        op2_test[4] = 2;
        op2_test[5] = 2;
        op2_test[6] = 666;
        op2_test[7] = 65535;

        for (unsigned i=0;i<TEST_SIZE;i++)
           risultato_test[i]=~(op1_test[i]&op2_test[i]); //ADD

    }


};

int sc_main(int argc, char* argv[])
{
  TestBench test("test");

  cout << "START TEST" << endl << endl;

  sc_start();

  return test.check();
}
